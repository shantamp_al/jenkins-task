# Jenkins CI Task

### Pre Requisites

- Repository on Bitbucket
- Jenkins Server

Step 1 = 
 
 Start Jenkins in the machine and log in.

 Start new item (project)

 # Jenkins CD Task

 - Once logged into Jenkins and CI pipeline is running correctly, now create an EC2 machine, give cohort9 SG and give ch9 shared key pair.
 - create a new pipeline (Freestyle project).
 - Give Desc, to run after CI job. (Discard old buils, 2)
 - Source code is git and put git clone link.
 - Credentials will be jenkins key.
 - Build Trigger - Build after other projects are built.
 - Add build step - Execute Shell

 ```Bash

 echo "Testing trigger"

 ```

 - Build now to trigger manually (previous job)
 - FYI - To check what work you have in the jenkins machine, you can go to workspace on the pipeline and you can see the files/folders transferred.
 - To send over the bashscript to run on production, configure the cd pipeline and on build step execute shell, you will need to scp the file over. USE Private IP to send over the file and don't forget to add 'StrictHostKeyChecking=no'.
 - If you receive an error regarding your host verification then check SSH is set in jenkins credentials and is noted on buiod environment.

